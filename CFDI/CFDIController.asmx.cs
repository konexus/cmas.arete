﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using CFDI.Controllers;
using System.IO;
using System.Xml.Linq;
using CFDI.Models;
using CFDI.CMAS;
namespace CFDI
{
    /// <summary>
    /// Summary description for CFDIController
    /// </summary>
    [WebService(Namespace = "http://www.integrasoftware.com.mx/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CFDIController : System.Web.Services.WebService
    {

        private ICmas _cmasrepository;
        private CFDIModels _objCFDI;
        public CFDIController()
        {
            _cmasrepository = new Cmas();
            _objCFDI = new CFDIModels();
        }

        //private CMASController ctrlCmas ;
        public enum eSAT
        {
            Vigente = 1,
            Cancelado = 0,
            NoEncontrado = -1,
            NoDisponible = -1,
        }

        public enum eRequest
        {
            SOLICITUDE = -1,
            OK = 0,
            ERP = 1,
        }


        [WebMethod]
        public string Cancel(string RFCEMISOR, string UUID)
        {
            string sResponse = string.Empty;
            string sfileName = string.Empty;
            string idError = string.Empty;
            string exceptionType = string.Empty;
            string description = string.Empty;
            string xml = string.Empty;
            string timbrelink = string.Empty;

            if (string.IsNullOrEmpty(RFCEMISOR) || string.IsNullOrEmpty(UUID))
                return "RFC EMISOR y/o UUID son necesarios";

            try
            {
                _objCFDI.sEmisorRFC = RFCEMISOR;
                _objCFDI.UUID = UUID;
                sfileName = UUID.Replace("-", "_");
                string usuarioIntegrador = "F751+m6fg1og+m1ujRDubw==";
                string folioFiscal = UUID.Trim().ToUpper();
                string sFechaCancelacion = string.Empty, sFolioFiscal = string.Empty, sEstadoCFI = string.Empty, sSelloDigitalSAT = string.Empty;
                int itry = 0;

                do
                {
                    // Call service PAC
                    using (ServicioProfact.Timbrado servicio = new ServicioProfact.Timbrado())
                    {

                        var vcancel = servicio.CancelaCFDI(usuarioIntegrador, RFCEMISOR, UUID);
                        var resultados = servicio.ObtieneCFDI(usuarioIntegrador, RFCEMISOR, UUID);
                        exceptionType = resultados[0].ToString();
                        idError = resultados[1].ToString();
                        description = string.IsNullOrEmpty(resultados[2].ToString()) ? "Factura cancelada satisfactoriamente" : resultados[2].ToString();
                        xml = resultados[3].ToString();
                        timbrelink = resultados[5].ToString();
                    }

                    //sResponse = string.Concat(numeroError, "|", descripcion);

                    if (int.Parse(idError) == 0 && !string.IsNullOrEmpty(xml))
                    {
                        itry = 100;
                        string sArchivoTemp = sfileName + "_cancel.xml";
                        string path = @System.Web.Hosting.HostingEnvironment.MapPath("~/XML/CANCEL/" + sArchivoTemp);

                        if (File.Exists(path))
                            File.Delete(@path);

                        System.IO.File.WriteAllText(@path + "", string.Concat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + xml));

                        XDocument xdoc = XDocument.Parse(string.Concat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + xml));

                        var vSignatureValue = (from p in xdoc.Descendants()
                                               where p.Name.LocalName == "SignatureValue"
                                               select p.Value);

                        sFechaCancelacion = (string)xdoc.Root.Attribute("Fecha").Value;

                        sFolioFiscal = UUID;
                        sEstadoCFI = "Cancelado";
                        sSelloDigitalSAT = (string)vSignatureValue.First().ToString();
                        DateTime dt = DateTime.Parse(sFechaCancelacion);

                        _objCFDI.iChoice = 2;
                        _cmasrepository.LogRequest(_objCFDI);
                    }
                    else
                    {
                        idError = "-100";
                        description = "Intente mas tarde " + description;
                        itry++;
                    }
                } while (itry <= 18);

                return sResponse = string.Concat("idError:", idError, "|tipoExcepcion:", exceptionType, "|descripcion:", description, "|xml:", xml, "|cadenaTimbre:", timbrelink, "|uuid:", UUID, "|");

            }
            catch (Exception ex)
            {
                //return sResponse = string.Concat("idError:", idError, "|tipoExcepcion:", exceptionType, "|descripcion:", description, "|xml:", xml, "|cadenaTimbre:", timbrelink, "|uuid:", UUID, "|");
                return sResponse = string.Concat("idError:-100|tipoExcepcion:|descripcion:Intente mas tarde ,Incidencia_", ex.Message, "|xml:|cadenaTimbre:|uuid:|");
            }

        }

        [WebMethod]
        public string CreateString(string xml_body, string RFC, string Optionals)
        {

            XmlDocument xDocpro = new XmlDocument();
            xDocpro.InnerXml = @xml_body;
            return Create(xDocpro, RFC, Optionals);
            //string sExceptionType = string.Empty;
            //string sIDError = string.Empty;
            //string sDescription = string.Empty;
            //string sXML = string.Empty;
            //string sCadenaTimbre = string.Empty;
            //string sResponse = string.Empty;
            //string sFileXML = string.Empty;
            //string UUID = string.Empty;
            //string sIDCFDI = string.Empty;
            //string sFileXMLOK = string.Empty;
            //try
            //{
            //    // ================================================================================================
            //    // LOG request and get id  set estatus like REQUEST
            //    //_objCFDI.sXML = xDocCFDI.InnerXml;
            //    _objCFDI.sXML = @xml_body;
            //    _objCFDI.iStatus = (int)eRequest.SOLICITUDE;
            //    _objCFDI.iChoice = 0;
            //    _objCFDI.sEmisorRFC = RFC;
            //    _objCFDI.ID = _cmasrepository.LogRequest(_objCFDI);

            //    // ================================================================================================
            //    // Validations
            //    if (string.IsNullOrEmpty(_objCFDI.sXML))
            //        return sResponse = string.Concat("idError:-100|tipoExcepcion:|descripcion:parametro XML es nullo o vacío|xml:|cadenaTimbre:|uuid:|");

            //    XmlDocument xDocCFDI = new XmlDocument();
            //    xDocCFDI.LoadXml(_objCFDI.sXML);
            //    if (!_cmasrepository.GetInfo(xDocCFDI, RFC, ref _objCFDI))
            //        return sResponse = string.Concat("idError:-100|tipoExcepcion:|descripcion:no autorizado para ," + _objCFDI.sEmisorRFC + "|xml:|cadenaTimbre:|uuid:|");

            //    // ================================================================================================
            //    // Save file 
            //    sIDCFDI = string.Concat(_objCFDI.sEmisorRFC, "_", _objCFDI.serie, "_", _objCFDI.folio);
            //    sFileXML = System.Web.Hosting.HostingEnvironment.MapPath("~/XML/" + sIDCFDI + ".xml");
            //    sFileXMLOK = System.Web.Hosting.HostingEnvironment.MapPath("~/XML/OK/" + sIDCFDI + ".xml");
            //    _objCFDI.sPATH = sFileXMLOK;
            //    xDocCFDI.Save(@sFileXML);

            //    //CFDI  PAT Process
            //    string userIntegrator = "F751+m6fg1og+m1ujRDubw==";
            //    string receiptBase64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(sFileXML));

            //    ServicioProfact.Timbrado servicio = new ServicioProfact.Timbrado();
            //    var serviceresponse = servicio.TimbraCFDI(userIntegrator, receiptBase64, sIDCFDI);

            //    sExceptionType = serviceresponse[0].ToString();
            //    //Numero de error
            //    sIDError = serviceresponse[1].ToString();
            //    //Descripcion del resultado
            //    sDescription = serviceresponse[2].ToString();
            //    //Xml timbrado
            //    sXML = serviceresponse[3].ToString();
            //    //Codigo bidimensional
            //    //byte[] codigoBidimensional = (byte[])(resultados[4]);
            //    //Cadena timbre
            //    sCadenaTimbre = serviceresponse[5].ToString();

            //    if (sIDError == "0")
            //    {
            //        // ================================================================================================
            //        // LOG request and get id  set estatus like PARTIAL
            //        UUID = sCadenaTimbre.Split('|')[3];
            //        _objCFDI.UUID = UUID;
            //        _objCFDI.iStatus = (int)eRequest.OK;
            //        _objCFDI.iChoice = 1;
            //        _cmasrepository.LogRequest(_objCFDI);
            //        xDocCFDI.InnerXml = @sXML;
            //        xDocCFDI.Save(sFileXMLOK);
            //        // if is true set estatus id 1 
            //        _cmasrepository.InsertCXC(_objCFDI);
            //    }
            //    else
            //    {
            //        _objCFDI.iStatus = (int)eRequest.SOLICITUDE;
            //        _objCFDI.sIncidenceDeatails = sIDError + "_" + sDescription;
            //        _objCFDI.iChoice = 1;
            //        _cmasrepository.LogRequest(_objCFDI);
            //    }
            //    return sResponse = string.Concat("idError:", sIDError, "|tipoExcepcion:", sExceptionType, "|descripcion:", sDescription, "|xml:", sXML, "|cadenaTimbre:", sCadenaTimbre, "|uuid:", UUID, "|");
            //}
            //catch (Exception ex)
            //{
            //    return sResponse = string.Concat("idError:-100|tipoExcepcion:|descripcion:Incidencia_", ex.Message, "|xml:|cadenaTimbre:|uuid:|");
            //}
        }



        /// <summary>
        /// Servicio CFDI Temporal
        /// </summary>
        /// <param name="xDocCFDI">XML Base 64</param>
        /// <param name="RFC">RFC USUARIO</param>
        /// <param name="Optionals">Configuraciones</param>
        /// <returns>idError:|tipoExcepcion:|descripcion:|xml:|cadenaTimbre:|uuid:|</returns>
        [WebMethod]
        public string Create(System.Xml.XmlDocument xDocCFDI, string RFC, string Optionals)
        {
            string sExceptionType = string.Empty;
            string sIDError = string.Empty;
            string sDescription = string.Empty;
            string sXML = string.Empty;
            string sCadenaTimbre = string.Empty;
            string sResponse = string.Empty;
            string sFileXML = string.Empty;
            string UUID = string.Empty;
            string sIDCFDI = string.Empty;
            string sFileXMLOK = string.Empty;
            try
            {
                // ================================================================================================
                // LOG request and get id  set estatus like REQUEST
                _objCFDI.sXML = xDocCFDI.InnerXml;
                _objCFDI.iStatus = (int)eRequest.SOLICITUDE;
                _objCFDI.iChoice = 0;
                _objCFDI.sEmisorRFC = RFC;
                _objCFDI.ID = _cmasrepository.LogRequest(_objCFDI);

                // ================================================================================================
                // Validations
                if (string.IsNullOrEmpty(xDocCFDI.InnerXml))
                    return sResponse = string.Concat("idError:-100|tipoExcepcion:|descripcion:parametro XML es nullo o vacío|xml:|cadenaTimbre:|uuid:|");

                if (!_cmasrepository.GetInfo(xDocCFDI, RFC, ref _objCFDI))
                    return sResponse = string.Concat("idError:-100|tipoExcepcion:|descripcion:no autorizado para ," + _objCFDI.sEmisorRFC + "|xml:|cadenaTimbre:|uuid:|");

                // ================================================================================================
                // Save file 
                sIDCFDI = string.Concat(_objCFDI.sEmisorRFC, "_", _objCFDI.serie, "_", _objCFDI.folio);
                sFileXML = System.Web.Hosting.HostingEnvironment.MapPath("~/XML/" + sIDCFDI + ".xml");
                sFileXMLOK = System.Web.Hosting.HostingEnvironment.MapPath("~/XML/OK/" + sIDCFDI + ".xml");
                _objCFDI.sPATH = sFileXMLOK;
                xDocCFDI.Save(@sFileXML);

                //CFDI  PAT Process
                string userIntegrator = "F751+m6fg1og+m1ujRDubw==";
                string receiptBase64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(sFileXML));

                ServicioProfact.Timbrado servicio = new ServicioProfact.Timbrado();
                var serviceresponse = servicio.TimbraCFDI(userIntegrator, receiptBase64, sIDCFDI);

                sExceptionType = serviceresponse[0].ToString();
                //Numero de error
                sIDError = serviceresponse[1].ToString();
                //Descripcion del resultado
                sDescription = serviceresponse[2].ToString();
                //Xml timbrado
                sXML = serviceresponse[3].ToString();
                //Codigo bidimensional
                //byte[] codigoBidimensional = (byte[])(resultados[4]);
                //Cadena timbre
                sCadenaTimbre = serviceresponse[5].ToString();

                if (sIDError == "0")
                {

                    // ================================================================================================
                    // LOG request and get id  set estatus like PARTIAL
                    UUID = sCadenaTimbre.Split('|')[3];
                    _objCFDI.UUID = UUID;
                    _objCFDI.iStatus = (int)eRequest.OK;
                    _objCFDI.iChoice = 1;
                    _cmasrepository.LogRequest(_objCFDI);
                    xDocCFDI.InnerXml = @sXML;
                    xDocCFDI.Save(sFileXMLOK);
                    // if is true set estatus id 1 
                    _cmasrepository.InsertCXC(_objCFDI);
                    _cmasrepository.LogRequest(_objCFDI);
                }
                else
                {
                    _objCFDI.iStatus = (int)eRequest.SOLICITUDE;
                    _objCFDI.sIncidenceDeatails = sIDError + "_" + sDescription;
                    _objCFDI.iChoice = 1;
                    _cmasrepository.LogRequest(_objCFDI);
                }

                return sResponse = string.Concat("idError:", sIDError, "|tipoExcepcion:", sExceptionType, "|descripcion:", sDescription, "|xml:", sXML, "|cadenaTimbre:", sCadenaTimbre, "|uuid:", UUID, "|");
            }
            catch (Exception ex)
            {
                return sResponse = string.Concat("idError:-100|tipoExcepcion:|descripcion:Incidencia_", ex.Message, "|xml:|cadenaTimbre:|uuid:|");
            }
        }

    }
}

