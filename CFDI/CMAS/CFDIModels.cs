﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CFDI.CMAS
{
    public class CFDIModels
    {
        public string serie { get; set; }
        public string folio { get; set; }
        public decimal total { get; set; }
        public decimal subTotal { get; set; }

        public string moneda { get; set; } = "MNX";
        public string UUID { get; set; } = string.Empty;
        public decimal IVA { get; set; }
        public decimal dTasa_IVA { get; set; }
        public string tipoCambio { get; set; }

        public string sEmisorRFC { get; set; }
        public string sEmisorNombre { get; set; }
        public string sEmisorCalle { get; set; }
        public string sEmisorCP { get; set; }

        public DateTime dFecha { get; set; }
        public string sReceptorRFC { get; set; }
        public string sReceptorNombre { get; set; }
        public string sReceptorCalle { get; set; }
        public string sReceptorCP { get; set; }
        public string sPATH { get; set; } = string.Empty;
        public string sXML { get; set; } = string.Empty;
        public string sOptionals { get; set; } = string.Empty;
        public string sIncidenceDeatails { get; set; } = string.Empty;
        public int iStatus { get; set; }
        public int iChoice { get; set; }
        public int ID { get; set; }
        public string sBD { get; set; }

    }
}