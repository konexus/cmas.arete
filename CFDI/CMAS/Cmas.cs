﻿using CFDI.Controllers;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using static CFDI.CFDIController;

namespace CFDI.CMAS
{
    public class Cmas : ICmas
    {
        public bool GetInfo(System.Xml.XmlDocument xDocCFDI, string RFC, ref CFDIModels objcfdi)
        {
            string cadena_original = string.Empty;
            XNamespace xcfdi = @"http://www.sat.gob.mx/cfd/3";
            XNamespace xtfd = @"http://www.sat.gob.mx/TimbreFiscalDigital";
            XDocument xdoc = ToXDocument(xDocCFDI);
            //CFDIModels objcfdi = new CFDIModels();
            var vroot = xdoc.Element(xcfdi + "Comprobante");
            var vEmisor = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Emisor");
            var vEmisorDomicilio = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Emisor").Element(xcfdi + "DomicilioFiscal");
            var vIVA = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Impuestos").Element(xcfdi + "Traslados");
            var vReceptor = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Receptor");
            var vReceptorDomicilio = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Receptor").Element(xcfdi + "Domicilio");

            objcfdi.serie = (string)vroot.Attribute("serie");
            objcfdi.folio = (string)vroot.Attribute("folio");
            objcfdi.total = (decimal)vroot.Attribute("total");
            objcfdi.subTotal = (decimal)vroot.Attribute("subTotal");
            objcfdi.moneda = (string)vroot.Attribute("Moneda") == null ? "MXN" : (string)vroot.Attribute("Moneda");
            objcfdi.tipoCambio = (string)vroot.Attribute("TipoCambio") == null ? "1.0000" : (string)vroot.Attribute("TipoCambio");
            objcfdi.dFecha = (DateTime)vroot.Attribute("fecha");

            if (vIVA.Elements().Count() > 1)
                objcfdi.IVA = 0;
            else
            {
                foreach (XElement element in vIVA.Descendants())
                {
                    string sTempImpuesto = (string)element.Attribute("impuesto");
                    string sTempTasa = string.Empty;

                    if (sTempImpuesto.ToUpper() == "IVA")
                    {
                        objcfdi.dTasa_IVA = decimal.Parse(element.Attribute("tasa").Value);
                        objcfdi.IVA = decimal.Parse(element.Attribute("importe").Value);
                    }
                }
            }

            objcfdi.sEmisorRFC = (string)vEmisor.Attribute("rfc");
            objcfdi.sReceptorRFC = (string)vReceptor.Attribute("rfc");
            objcfdi.sEmisorNombre = (string)vEmisor.Attribute("nombre");
            objcfdi.sReceptorNombre = (string)vReceptor.Attribute("nombre");
            //objcfdi.sEmisorCalle = (string)vEmisorDomicilio.Attribute("calle");
            //objcfdi.sEmisorCP = (string)vEmisorDomicilio.Attribute("codigoPostal");
            //objcfdi.sReceptorCalle = (string)vReceptorDomicilio.Attribute("calle");
            //objcfdi.sReceptorCP = (string)vReceptorDomicilio.Attribute("codigoPostal");

            if (objcfdi.sEmisorRFC == "CCS090723I81" || objcfdi.sEmisorRFC == "CED141007LF0" || objcfdi.sEmisorRFC == "INT040322HI3")
                if (Regex.IsMatch(objcfdi.sEmisorRFC, RFC))
                {

                    switch (objcfdi.sEmisorRFC)
                    {
                        case "CCS090723I81":
                        case "CED141007LF0":
                            objcfdi.sBD = "INTEGRA_CMAS";
                            break;
                        default:
                            objcfdi.sBD = "INTEGRA_DEMO";
                            break;
                    }
                    return true;
                }
            return false;
        }
        public bool InsertCXC(CFDIModels objCFDI)
        {
            DataTable dt = new DataTable();
            SQLConection context = new SQLConection();
            int iOffice = -1;
            try
            {
                if (!lfVerificaFolioCXC(1, objCFDI.UUID, objCFDI.sBD))
                {
                    iOffice = lfGetOffice(objCFDI.sBD);
                    context.Parametros.Clear();
                    context.Parametros.Add(new SqlParameter("@Numero", 1));
                    context.Parametros.Add(new SqlParameter("@Numero_Empresa", 1));
                    context.Parametros.Add(new SqlParameter("@RFC_Cliente", objCFDI.sReceptorRFC));
                    context.Parametros.Add(new SqlParameter("@Nombre_Cliente", objCFDI.sReceptorNombre));
                    context.Parametros.Add(new SqlParameter("@Serie", objCFDI.serie));
                    context.Parametros.Add(new SqlParameter("@Folio", objCFDI.folio));
                    context.Parametros.Add(new SqlParameter("@Fecha", objCFDI.dFecha));
                    context.Parametros.Add(new SqlParameter("@RFC_Emisor", objCFDI.sEmisorRFC));
                    context.Parametros.Add(new SqlParameter("@Monto", objCFDI.total));
                    context.Parametros.Add(new SqlParameter("@Moneda", objCFDI.moneda));
                    context.Parametros.Add(new SqlParameter("@Tipo_Cambio", objCFDI.tipoCambio));
                    context.Parametros.Add(new SqlParameter("@Tasa_IVA", objCFDI.dTasa_IVA));
                    context.Parametros.Add(new SqlParameter("@Folio_Fiscal", objCFDI.UUID));
                    context.Parametros.Add(new SqlParameter("@Generar_Cobro", "0"));
                    context.Parametros.Add(new SqlParameter("@Fecha_Cobro", objCFDI.dFecha));
                    context.Parametros.Add(new SqlParameter("@Folio_Cobro", ""));
                    context.Parametros.Add(new SqlParameter("@Monto_Cobro", "0"));
                    context.Parametros.Add(new SqlParameter("@Moneda_Cobro", "0"));
                    context.Parametros.Add(new SqlParameter("@Tipo_Cambio_Cobro", objCFDI.tipoCambio));
                    context.Parametros.Add(new SqlParameter("@Usuario", 2));
                    context.Parametros.Add(new SqlParameter("@Sucursal", iOffice));
                    context.Parametros.Add(new SqlParameter("@Monto_IVA", objCFDI.IVA));
                    context.Parametros.Add(new SqlParameter("@Monto_Subtotal", objCFDI.subTotal));
                    dt = context.ExecuteProcedure("[" + objCFDI.sBD + "].[dbo].[sp_Cuentas_Cobrar_Alta_CFDI]", true).Copy();
                    objCFDI.iChoice = 1;
                    return true;
                }
                objCFDI.iStatus = (int)eRequest.OK;
                objCFDI.iChoice = 1;
                // send message failured
                return false;
            }
            catch (Exception ex)
            {
                objCFDI.iStatus = (int)eRequest.SOLICITUDE;
                objCFDI.iChoice = 1;
                objCFDI.sIncidenceDeatails = ex.Message ;
                return false;
            }
            finally
            {
                objCFDI.sIncidenceDeatails = objCFDI.sIncidenceDeatails + " @precaution:" + string.Concat(" use [", objCFDI.sBD, "] execute sp_Cuentas_Cobrar_Alta_CFDI 1,1,'", objCFDI.sReceptorRFC, "',", "'", objCFDI.sReceptorNombre, "','", objCFDI.serie, "','", objCFDI.folio, "','", objCFDI.dFecha, "','", objCFDI.sEmisorRFC, "','", objCFDI.total, "','", objCFDI.moneda, "','", objCFDI.tipoCambio, "','", objCFDI.dTasa_IVA, "','", objCFDI.UUID, "','0','", objCFDI.dFecha, "','',0,0,'", objCFDI.tipoCambio, "','2','", iOffice, "',", objCFDI.IVA, ",", objCFDI.subTotal); objCFDI.iStatus = (int)eRequest.ERP;
                LogRequest(objCFDI);
            }
        }
        public int LogRequest(CFDIModels objCFDI)
        {
            SQLConection context = new SQLConection();
            DataTable dt = new DataTable();
            try
            {
                context.Parametros.Clear();
                context.Parametros.Add(new SqlParameter("@XMLBody", objCFDI.sXML));
                context.Parametros.Add(new SqlParameter("@UUID", objCFDI.UUID));
                context.Parametros.Add(new SqlParameter("@STATUS", objCFDI.iStatus));
                context.Parametros.Add(new SqlParameter("@RFC", objCFDI.sEmisorRFC));
                context.Parametros.Add(new SqlParameter("@PATH", objCFDI.sPATH));
                context.Parametros.Add(new SqlParameter("@OPTIONALS", objCFDI.sOptionals));
                context.Parametros.Add(new SqlParameter("@INCIDENCEDETAILS", objCFDI.sIncidenceDeatails));
                context.Parametros.Add(new SqlParameter("@CHOICE", objCFDI.iChoice));
                context.Parametros.Add(new SqlParameter("@ID", objCFDI.ID));
                dt = context.ExecuteProcedure("[INTEGRA_KONEXUS_FAC].[dbo].[sp_CXC_Log]", true).Copy();

                if (dt.Rows.Count > 0)
                    return Convert.ToInt32(dt.Rows[0][0]);
                return -1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }
        private XDocument ToXDocument(XmlDocument xDocCFDI)
        {
            using (var nodeReader = new XmlNodeReader(xDocCFDI))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }

        private int lfGetOffice(string sBD, int iEnterprise = 1, int iUser = 2)
        {
            int iOffice = 0;
            DataTable dt = new DataTable();
            SQLConection context = new SQLConection();
            try
            {
                context.Parametros.Clear();
                context.Parametros.Add(new SqlParameter("@companynumber", iEnterprise));
                context.Parametros.Add(new SqlParameter("@user", iUser));
                dt = context.ExecuteProcedure("[" + sBD + "].[dbo].[sp_Controles_ObtieneOficinas_v1]", true).Copy();
                if (dt.Rows.Count > 0)
                    iOffice = (int)dt.Rows[0][0];
                return iOffice;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
        private bool lfVerificaFolioCXC(int Numero_Empresa, string sFolio_Fiscal, string sBD)
        {

            DataTable dt = new DataTable();
            SQLConection context = new SQLConection();
            string sQuery = "USE [" + sBD + "] SELECT dbo.fn_Verifica_Folios_Fiscales_CxC_v2(" + Numero_Empresa + "," + " '" + sFolio_Fiscal + "' " + ") AS Existe";
            dt = context.ExecuteQuery(sQuery).Copy();
            if (dt.Rows.Count > 0)
                return Convert.ToBoolean(dt.Rows[0][0]);
            return false;
        }
    }
}