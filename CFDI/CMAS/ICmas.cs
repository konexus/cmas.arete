﻿namespace CFDI.CMAS
{
    interface ICmas
    {
        bool GetInfo(System.Xml.XmlDocument xDocCFDI, string RFC, ref CFDIModels objcfdi);
        bool InsertCXC(CFDIModels objCFDI);
        int LogRequest(CFDIModels objCFDI);
    }
}
