﻿using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Linq;
using CFDI.Models;
namespace CFDI.Controllers
{
    public static class CMASController
    {
        /// <summary>
        /// review XML info
        /// </summary>
        /// <param name="xDocCFDI"></param>
        /// <param name="RFC"></param>
        /// <returns></returns>
        public static CFDIModel Credentials(XmlDocument xDocCFDI, string RFC)
        {
            string cadena_original = string.Empty;
            XNamespace xcfdi = @"http://www.sat.gob.mx/cfd/3";
            XNamespace xtfd = @"http://www.sat.gob.mx/TimbreFiscalDigital";
            XDocument xdoc = ToXDocument(xDocCFDI);
            CFDIModel objcfdi = new CFDIModel();
            var vroot = xdoc.Element(xcfdi + "Comprobante");
            var vEmisor = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Emisor");
            var vEmisorDomicilio = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Emisor").Element(xcfdi + "DomicilioFiscal");
            var vIVA = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Impuestos").Element(xcfdi + "Traslados");
            //var elementoiva = xdoc.Element(cfdi + "Comprobante").Element(cfdi + "Impuestos").Element(cfdi + "Traslados");

            var vReceptor = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Receptor");
            var vReceptorDomicilio = xdoc.Element(xcfdi + "Comprobante").Element(xcfdi + "Receptor").Element(xcfdi + "Domicilio");

            objcfdi.serie = (string)vroot.Attribute("serie");
            objcfdi.folio = (string)vroot.Attribute("folio");
            objcfdi.total = (decimal)vroot.Attribute("total");
            objcfdi.subTotal = (decimal)vroot.Attribute("subTotal");
            objcfdi.moneda = (string)vroot.Attribute("Moneda");
            objcfdi.tipoCambio = (string)vroot.Attribute("TipoCambio");
            objcfdi.dFecha = (DateTime)vroot.Attribute("fecha");

            if (vIVA.Elements().Count() > 1)
                objcfdi.IVA = 0;
            else
            {
                foreach (XElement element in vIVA.Descendants())
                {
                    string sTempImpuesto = (string)element.Attribute("impuesto");
                    string sTempTasa = string.Empty;

                    if (sTempImpuesto.ToUpper() == "IVA")
                    {
                        objcfdi.IVA = decimal.Parse(element.Attribute("importe").Value);
                    }
                }
            }


            objcfdi.sEmisorRFC = (string)vEmisor.Attribute("rfc");
            objcfdi.sEmisorNombre = (string)vEmisor.Attribute("nombre");
              
            //objcfdi.sEmisorCalle = (string)vEmisorDomicilio.Attribute("calle");
            //objcfdi.sEmisorCP = (string)vEmisorDomicilio.Attribute("codigoPostal");

            objcfdi.sReceptorRFC = (string)vReceptor.Attribute("rfc");
            objcfdi.sReceptorNombre = (string)vReceptor.Attribute("nombre");
            //objcfdi.sReceptorCalle = (string)vReceptorDomicilio.Attribute("calle");
            //objcfdi.sReceptorCP = (string)vReceptorDomicilio.Attribute("codigoPostal");

            if (objcfdi.sEmisorRFC == "CCS090723I81" || objcfdi.sEmisorRFC == "CED141007LF0" || objcfdi.sEmisorRFC == "INT040322HI3")
                if (Regex.IsMatch(objcfdi.sEmisorRFC, RFC))
                    return objcfdi;
            return null;
        }

        private static XDocument ToXDocument(this XmlDocument xmlDocument)
        {
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }
        public static void LOG(CFDIModel objCFDI)
        {
            string sparameters = string.Concat(objCFDI.serie, "|", objCFDI.folio, "|", objCFDI.total, "|", objCFDI.subTotal, "|", objCFDI.moneda, "|", objCFDI.IVA, "|", objCFDI.tipoCambio, "|", objCFDI.sEmisorRFC, "|", objCFDI.sEmisorNombre, "|", objCFDI.sEmisorCalle, "|", objCFDI.sEmisorCP, "|", objCFDI.sReceptorRFC, "|", objCFDI.sReceptorNombre, "|", objCFDI.dFecha, "|", objCFDI.sReceptorRFC, "|", objCFDI.sReceptorNombre, "|", objCFDI.sReceptorCalle, "|", objCFDI.sReceptorCP, "|");
            SQLConection context = new SQLConection();
            DataTable dt = new DataTable();
            context.Parametros.Clear();
            context.Parametros.Add(new SqlParameter("@UUID", objCFDI.UUID));
            context.Parametros.Add(new SqlParameter("@RUTA", objCFDI.sPATH));
            context.Parametros.Add(new SqlParameter("@PARAMETROS", sparameters));
            dt = context.ExecuteProcedure("[sp_LOG_FacturaRegistroError]", true).Copy();
        }

        public static DataTable sp_Cuentas_Cobrar_Alta_CFDI2(int? iNumero,
                                                             int iNumero_Empresa,
                                                             string sRfcCliente,
                                                             string sNombreCliente,
                                                             string sSerie,
                                                             string sFolio,
                                                             DateTime Fecha,
                                                             string sRfcEmisor,
                                                             decimal dMonto,
                                                             string sMoneda,
                                                             decimal dTipoCambio,
                                                             decimal dTasaIva,
                                                             string sFolioFiscal,
                                                             int iGenerarCobro,
                                                             DateTime? FechaCobro,
                                                             string sFolioCobro,
                                                             decimal? dMontoCobro,
                                                             int? iMonedaCobro,
                                                             decimal? dTipoCambioCobro,
                                                             int iUsuario,
                                                             int? iSucursal,
                                                             decimal? dMontoIva,
                                                             decimal? dMontoSubtotal)
        {

            DataTable dt = new DataTable();
            SQLConection context = new SQLConection();
            context.Parametros.Clear();
            context.Parametros.Add(new SqlParameter("@Numero", iNumero));
            context.Parametros.Add(new SqlParameter("@Numero_Empresa", iNumero_Empresa));
            context.Parametros.Add(new SqlParameter("@RFC_Cliente", sRfcCliente));
            context.Parametros.Add(new SqlParameter("@Nombre_Cliente", sNombreCliente));
            context.Parametros.Add(new SqlParameter("@Serie", sSerie));
            context.Parametros.Add(new SqlParameter("@Folio", sFolio));
            context.Parametros.Add(new SqlParameter("@Fecha", Fecha));
            context.Parametros.Add(new SqlParameter("@RFC_Emisor", sRfcEmisor));
            context.Parametros.Add(new SqlParameter("@Monto", dMonto));
            context.Parametros.Add(new SqlParameter("@Moneda", sMoneda));
            context.Parametros.Add(new SqlParameter("@Tipo_Cambio", dTipoCambio));
            context.Parametros.Add(new SqlParameter("@Tasa_IVA", dTasaIva));
            context.Parametros.Add(new SqlParameter("@Folio_Fiscal", sFolioFiscal));
            context.Parametros.Add(new SqlParameter("@Generar_Cobro", iGenerarCobro));
            context.Parametros.Add(new SqlParameter("@Fecha_Cobro", FechaCobro.HasValue ? (object)FechaCobro.Value : DBNull.Value));
            context.Parametros.Add(new SqlParameter("@Folio_Cobro", sFolioCobro));
            context.Parametros.Add(new SqlParameter("@Monto_Cobro", dMontoCobro.HasValue ? (object)dMontoCobro.Value : DBNull.Value));
            context.Parametros.Add(new SqlParameter("@Moneda_Cobro", iMonedaCobro.HasValue ? (object)iMonedaCobro.Value : DBNull.Value));
            context.Parametros.Add(new SqlParameter("@Tipo_Cambio_Cobro", dTipoCambioCobro.HasValue ? (object)dTipoCambioCobro.Value : DBNull.Value));
            context.Parametros.Add(new SqlParameter("@Usuario", iUsuario));
            context.Parametros.Add(new SqlParameter("@Sucursal", iSucursal.HasValue ? (object)iSucursal.Value : DBNull.Value));
            context.Parametros.Add(new SqlParameter("@Monto_IVA", dMontoIva.HasValue ? (object)dMontoIva.Value : DBNull.Value));
            context.Parametros.Add(new SqlParameter("@Monto_Subtotal", dMontoSubtotal.HasValue ? (object)dMontoSubtotal.Value : DBNull.Value));
            dt = context.ExecuteProcedure("[dbo].[INTEGRA_DEMO].[sp_Cuentas_Cobrar_Alta_CFDI]", true).Copy();
            return dt;
        }

        public static DataTable sp_Cuentas_Cobrar_Alta_CFDI2(CFDIModel objcfdi)
        {

            DataTable dt = new DataTable();
            SQLConection context = new SQLConection();
            context.Parametros.Clear();
            context.Parametros.Add(new SqlParameter("@Numero", 0));
            context.Parametros.Add(new SqlParameter("@Numero_Empresa", 1));
            context.Parametros.Add(new SqlParameter("@RFC_Cliente", objcfdi.sReceptorRFC));
            context.Parametros.Add(new SqlParameter("@Nombre_Cliente", objcfdi.sReceptorNombre));
            context.Parametros.Add(new SqlParameter("@Serie", objcfdi.serie));
            context.Parametros.Add(new SqlParameter("@Folio", objcfdi.folio));
            context.Parametros.Add(new SqlParameter("@Fecha", objcfdi.dFecha));
            context.Parametros.Add(new SqlParameter("@RFC_Emisor", objcfdi.sEmisorRFC));
            context.Parametros.Add(new SqlParameter("@Monto", objcfdi.total));
            context.Parametros.Add(new SqlParameter("@Moneda", objcfdi.moneda));
            context.Parametros.Add(new SqlParameter("@Tipo_Cambio", objcfdi.tipoCambio));
            context.Parametros.Add(new SqlParameter("@Tasa_IVA", objcfdi.IVA));
            context.Parametros.Add(new SqlParameter("@Folio_Fiscal", objcfdi.UUID));
            context.Parametros.Add(new SqlParameter("@Generar_Cobro", 0));
            context.Parametros.Add(new SqlParameter("@Fecha_Cobro", DateTime.Now));
            context.Parametros.Add(new SqlParameter("@Folio_Cobro", string.Empty));
            context.Parametros.Add(new SqlParameter("@Monto_Cobro", objcfdi.total));
            context.Parametros.Add(new SqlParameter("@Moneda_Cobro", objcfdi.moneda));
            context.Parametros.Add(new SqlParameter("@Tipo_Cambio_Cobro", objcfdi.tipoCambio));
            context.Parametros.Add(new SqlParameter("@Usuario", 2));
            context.Parametros.Add(new SqlParameter("@Sucursal", 1));
            context.Parametros.Add(new SqlParameter("@Monto_IVA", objcfdi.IVA));
            context.Parametros.Add(new SqlParameter("@Monto_Subtotal", objcfdi.subTotal));
            dt = context.ExecuteProcedure("[INTEGRA_DEMO].[dbo].[sp_Cuentas_Cobrar_Alta_CFDI]", true).Copy();
            //"[" + sBD + "].[dbo].[sp_Factura_ConsultaNumeroReferencia]
            return dt;
        }


        public static void test_sp_Cuentas_Cobrar_Alta_CFDI2()
        {

            //sp_Cuentas_Cobrar_Alta_CFDI2(0, 1,);
        }
    }


}