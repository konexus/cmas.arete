﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CFDI.Models
{
    public class CFDIModel
    {
        public string serie { get; set; }
        public string folio { get; set; }
        public decimal total { get; set; }
        public decimal subTotal { get; set; }
        public string moneda { get; set; }
        public string UUID { get; set; }
        public decimal IVA { get; set; }
        public string tipoCambio { get; set; }

        public string sEmisorRFC { get; set; }
        public string sEmisorNombre { get; set; }
        public string sEmisorCalle { get; set; }
        public string sEmisorCP { get; set; }

        public DateTime dFecha { get; set; }
        public string sReceptorRFC { get; set; }
        public string sReceptorNombre { get; set; }
        public string sReceptorCalle { get; set; }
        public string sReceptorCP { get; set; }
        public string sPATH { get; set; }

    }
}